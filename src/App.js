import { Route, Routes } from "react-router-dom";
import { QueryClientProvider } from "@tanstack/react-query";

import Layout from "./components/Layout";

import HomePage from "./pages/HomePage";
import MissingPage from "./pages/MissingPage";
import CustomToaster from "./components/Toaster";

import queryClient from "./services/QueryService";

function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <Routes>
        <Route path="/" element={<Layout />}>
          {/* public routes */}
          <Route path="/" element={<HomePage />} />

          {/* catch all */}
          <Route path="*" element={<MissingPage />} />
        </Route>
      </Routes>
      <CustomToaster />
    </QueryClientProvider>
  );
}

export default App;
