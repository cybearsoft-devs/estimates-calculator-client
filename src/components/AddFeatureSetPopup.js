import React from "react";
import { useState, useEffect } from "react";
import classNames from "classnames";

import Button from "./Button";
import Checkbox from "./Checkbox";
import Separator from "./Separator";
import SpanWithTooltip from "./SpanWithTooltip";

import { ReactComponent as CloseIcon } from "../assets/icons/close_icon.svg";

function AddFeatureSetPopup({
  features,
  selectedFeatures,
  isOpen,
  onCancel,
  onAdd,
}) {
  const [pendingSelectedFeatures, setPendingSelectedFeatures] = useState([]);

  useEffect(() => {
    setPendingSelectedFeatures(selectedFeatures);
  }, [selectedFeatures]);

  const handleFeatureSelect = (feature) => {
    if (
      pendingSelectedFeatures.some(
        (selectedFeature) => selectedFeature.id === feature.id
      )
    ) {
      setPendingSelectedFeatures((prevSelectedFeatures) =>
        prevSelectedFeatures.filter(
          (selectedFeature) => selectedFeature.id !== feature.id
        )
      );
    } else {
      setPendingSelectedFeatures((prevSelectedFeatures) => [
        ...prevSelectedFeatures,
        {
          ...feature,
          newEstimates: {
            backend: {
              min: "",
              max: "",
            },
            frontend: {
              min: "",
              max: "",
            },
            ios: {
              min: "",
              max: "",
            },
            android: {
              min: "",
              max: "",
            },
            crossPlatform: {
              min: "",
              max: "",
            },
          },
        },
      ]);
    }
  };

  const handleAddFeatures = () => {
    onAdd(pendingSelectedFeatures);
  };

  const overlayClassnames = classNames("overlay", {
    show: isOpen,
  });

  const popupClassnames = classNames("add-popup", {
    show: isOpen,
  });

  const itemIsChecked = (feature) => {
    return pendingSelectedFeatures.some(
      (selectedFeature) => selectedFeature.id === feature.id
    );
  };

  const renderFeatures = () => {
    return features.map((feature) => (
      <React.Fragment key={feature.id}>
        <div
          className={`feature-item ${itemIsChecked(feature) ? "selected" : ""}`}
        >
          <Checkbox
            checked={itemIsChecked(feature)}
            onClick={() => handleFeatureSelect(feature)}
          />
          <SpanWithTooltip
            text={feature.name}
            tooltipId={feature.id + "-feature-tooltip"}
          />
        </div>
      </React.Fragment>
    ));
  };

  return (
    <>
      <div className={overlayClassnames} />
      <div className={popupClassnames}>
        <div className="add-popup-header">
          <h1>Add feature set</h1>
          <CloseIcon onClick={onCancel} />
        </div>
        <div className="add-popup-content">
          <Separator width="111%" />
          <div className="feature-items-list">{renderFeatures()}</div>
        </div>
        <div className="add-popup-buttons">
          <Button secondary height="2.5rem" width="13rem" onClick={onCancel}>
            Cancel
          </Button>
          <Button
            primary
            height="2.5rem"
            width="13rem"
            onClick={handleAddFeatures}
          >
            Add
          </Button>
        </div>
      </div>
    </>
  );
}

export default AddFeatureSetPopup;
