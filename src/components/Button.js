import classNames from "classnames";

function Button({ children, primary, secondary, width, height, ...rest }) {
  const btnClasses = classNames("btn", {
    "btn-primary": primary,
    "btn-secondary": secondary,
  });

  const btnDimensions = {
    width: width || "auto",
    height: height || "auto",
  };

  return (
    <button
      type="button"
      {...rest}
      className={btnClasses}
      style={btnDimensions}
    >
      {children}
    </button>
  );
}

Button.propTypes = {
  checkVariationValue: ({ primary, secondary }) => {
    const count = Number(!!primary) + Number(!!secondary);
    if (count > 1) return new Error("Invalid props.");
  },
};

export default Button;
