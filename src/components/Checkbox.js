import { ReactComponent as CheckedIcon } from "../assets/icons/checked_icon.svg";
import { ReactComponent as NotCheckedIcon } from "../assets/icons/not_checked_icon.svg";

function Checkbox({ checked, onClick, ...rest }) {
  return (
    <div className="checkbox__container" onClick={onClick}>
      <input type="checkbox" checked={checked} onChange={onClick} {...rest} />
      <span className="checkbox__checkmark">
        {checked ? <CheckedIcon /> : <NotCheckedIcon />}
      </span>
    </div>
  );
}

export default Checkbox;
