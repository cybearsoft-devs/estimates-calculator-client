import React from "react";
import classNames from "classnames";

import Button from "./Button";
import Separator from "./Separator";

import { ReactComponent as CloseIcon } from "../assets/icons/close_icon.svg";

function DeleteFeatureSetPopup({ isOpen, onNo, onYes }) {
  const overlayClassnames = classNames("overlay", {
    show: isOpen,
  });

  const popupClassnames = classNames("delete-popup", {
    show: isOpen,
  });

  return (
    <>
      <div className={overlayClassnames} />
      <div className={popupClassnames}>
        <div className="delete-popup-header">
          <h1>Delete feature set</h1>
          <CloseIcon onClick={onNo} />
        </div>
        <div className="delete-popup-content">
          <Separator width="111%" />
          <span>Are you sure you want to delete the feature set?</span>
        </div>
        <div className="delete-popup-buttons">
          <Button secondary height="2.5rem" width="16rem" onClick={onNo}>
            No
          </Button>
          <Button primary height="2.5rem" width="16rem" onClick={onYes}>
            Yes
          </Button>
        </div>
      </div>
    </>
  );
}

export default DeleteFeatureSetPopup;
