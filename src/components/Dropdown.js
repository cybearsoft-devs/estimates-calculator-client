import PropTypes from "prop-types";
import React, { useState, useRef, useEffect } from "react";
import classNames from "classnames";

function Dropdown({ options, selectedOption, onSelect }) {
  const [isOpen, setIsOpen] = useState(false);
  const dropdownToggleButtonRef = useRef(null);
  const dropdownContainerRef = useRef(null);

  const handleToggleClick = () => {
    setIsOpen(!isOpen);
  };

  const handleDocumentClick = (event) => {
    if (
      dropdownContainerRef.current &&
      !dropdownContainerRef.current.contains(event.target)
    ) {
      setIsOpen(false);
    }
  };

  useEffect(() => {
    document.addEventListener("click", handleDocumentClick);

    return () => {
      document.removeEventListener("click", handleDocumentClick);
    };
  }, []);

  const dropDownClassNames = classNames("dropdown", {
    opened: isOpen,
    selected: selectedOption.id !== 0,
    disabled: options?.length <= 0,
  });

  return (
    <div ref={dropdownContainerRef} className={dropDownClassNames}>
      <button
        ref={dropdownToggleButtonRef}
        onClick={handleToggleClick}
        className="dropdown-toggle"
        disabled={options?.length <= 0}
      >
        <div className="dropdown-toggle-content">
          <span>{selectedOption.name_of_technology}</span>
          <div className="dropdown-arrow" />
        </div>
      </button>
      {isOpen && (
        <ul className="dropdown-list">
          {options.map((option, index) => (
            <React.Fragment key={option.id}>
              <li
                key={option.id}
                onClick={() => {
                  onSelect(option);
                  setIsOpen(false);
                }}
                className="dropdown-item"
              >
                <span>{option.name_of_technology}</span>
              </li>
              {index < options.length - 1 && <hr className="horizontal-line" />}
            </React.Fragment>
          ))}
        </ul>
      )}
    </div>
  );
}

Dropdown.propTypes = {
  options: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      name_of_technology: PropTypes.string.isRequired,
    })
  ).isRequired,
  onSelect: PropTypes.func.isRequired,
};

export default Dropdown;
