import { useState } from "react";
import classNames from "classnames";

import Input from "./Input";
import Button from "./Button";
import Spinner from "./Spinner";

import { isValidEmail } from "../utils";

function InputLabel({ label }) {
  return <span className="form-input-label">{label}</span>;
}

function InputError({ error }) {
  return <p className="form-input-error">{error}</p>;
}

function TextInput({ label, value, error, onChange, ...rest }) {
  const inputClasses = classNames("form-input", {
    "form-input--filled": value,
    "form-input--error": error && !value,
  });

  return (
    <div className="form-input-container">
      {value && <InputLabel label={label} />}
      <Input
        type="text"
        value={value}
        onChange={onChange}
        placeholder={label}
        className={inputClasses}
        {...rest}
      />
      {error && <InputError error={error} />}
    </div>
  );
}

function GetEstimatesForm({ onSubmit, isLoading }) {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");

  const [nameError, setNameError] = useState("");
  const [emailError, setEmailError] = useState("");

  const handleNameChange = (e) => {
    setName(e.target.value);
    setNameError("");
  };

  const handleEmailChange = (e) => {
    setEmail(e.target.value);
    setEmailError("");
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (!name) {
      setName("");
      setNameError("Please, enter your Name");
      return;
    }

    if (!email) {
      setEmail("");
      setEmailError("Please, enter your Work Email");
      return;
    }

    if (!isValidEmail(email)) {
      setEmail("");
      setEmailError("Please, enter a valid email address");
      return;
    }

    const onSuccess = () => {
      setName("");
      setEmail("");
    };

    onSubmit(name, email, onSuccess);
  };

  return (
    <form onSubmit={handleSubmit}>
      <div className="form-inputs">
        <TextInput
          label="Name"
          value={name}
          error={nameError}
          disabled={isLoading}
          onChange={handleNameChange}
        />
        <TextInput
          label="Work email"
          value={email}
          error={emailError}
          disabled={isLoading}
          onChange={handleEmailChange}
        />
      </div>

      <Button
        primary
        type="submit"
        disabled={!(name || email) || isLoading}
        width="25rem"
        height="2.5rem"
      >
        {isLoading ? (
          <Spinner width="1.5rem" height="1.5rem" />
        ) : (
          "Get Estimate"
        )}
      </Button>
    </form>
  );
}

export default GetEstimatesForm;
