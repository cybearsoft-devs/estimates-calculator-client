import React from "react";

function Input({ type, value, onChange, children, ...rest }) {
  return (
    <input type={type} value={value} onChange={onChange} {...rest}>
      {children}
    </input>
  );
}

export default Input;
