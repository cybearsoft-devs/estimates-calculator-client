function Separator({ width, height }) {
  const separatorDimensions = {
    width: width || "100%",
    height: height || "0.063rem",
  };

  return <div className="separator" style={separatorDimensions}></div>;
}

export default Separator;
