import { useEffect, useRef } from "react";
import { Tooltip as ReactTooltip } from "react-tooltip";

import { isTextOverflowing } from "../utils";

function SpanWithTooltip({ text, tooltipId }) {
  const spanRef = useRef(null);

  useEffect(() => {
    const spanElement = spanRef.current;
    if (spanElement && isTextOverflowing(spanElement)) {
      spanElement.setAttribute("data-tooltip-id", tooltipId);
      spanElement.setAttribute("data-tooltip-content", text);
    }
  }, [text, tooltipId]);

  return (
    <>
      <ReactTooltip
        id={tooltipId}
        place="bottom-start"
        variant="info"
        className="tooltip"
      />
      <span ref={spanRef}>{text}</span>
    </>
  );
}

export default SpanWithTooltip;
