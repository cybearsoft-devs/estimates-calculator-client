const Spinner = ({ width, height }) => {
  const spinnerDimensions = {
    width: width || "auto",
    height: height || "auto",
  };

  return (
    <div className="spinner-container">
      <div className="spinner" style={spinnerDimensions}></div>
    </div>
  );
};

export default Spinner;
