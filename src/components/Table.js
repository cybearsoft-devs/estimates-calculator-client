import React from "react";
import classNames from "classnames";

import { useTechnologies } from "../features/technologies/useTechnologies";

import Input from "./Input";
import Button from "./Button";
import Dropdown from "./Dropdown";
import SpanWithTooltip from "./SpanWithTooltip";

import { ReactComponent as DeleteIcon } from "../assets/icons/delete_icon.svg";

function MinCellInput({ itemId, platform, data, setData }) {
  const handleChange = (e) => {
    const value = e.target.value;
    const numericInput = value.replace(/[^0-9]/g, "");

    const updatedData = data.map((item) => {
      if (item.id === itemId) {
        const newMin = Number(numericInput);
        return {
          ...item,
          newEstimates: {
            ...item.newEstimates,
            [platform]: {
              ...item.newEstimates[platform],
              min: newMin,
            },
          },
        };
      }
      return item;
    });
    setData(updatedData);
  };

  const handleUpdateFinish = () => {
    const newMin =
      data.find((item) => item.id === itemId)?.newEstimates[platform]?.min ||
      "";

    const updatedData = data.map((item) => {
      if (item.id === itemId) {
        const newMax = Math.max(newMin, item.newEstimates[platform].max);
        return {
          ...item,
          newEstimates: {
            ...item.newEstimates,
            [platform]: {
              ...item.newEstimates[platform],
              max: newMax,
            },
          },
        };
      }
      return item;
    });
    if (newMin !== "") {
      setData(updatedData);
    }
  };

  const handleKeyDown = (e) => {
    if (e.key === "Enter") {
      e.target.blur();
    }
  };

  return (
    <Input
      type="text"
      className="estimate-cell-input"
      placeholder="min"
      value={
        data.find((item) => item.id === itemId)?.newEstimates[platform]?.min
      }
      onChange={handleChange}
      onBlur={handleUpdateFinish}
      onKeyDown={handleKeyDown}
    />
  );
}

function MaxCellInput({ itemId, platform, data, setData }) {
  const handleChange = (e) => {
    const value = e.target.value;
    const numericInput = value.replace(/[^0-9]/g, "");

    const updatedData = data.map((item) => {
      if (item.id === itemId) {
        const newMax = Number(numericInput);
        return {
          ...item,
          newEstimates: {
            ...item.newEstimates,
            [platform]: {
              ...item.newEstimates[platform],
              max: newMax,
            },
          },
        };
      }
      return item;
    });
    setData(updatedData);
  };

  const handleUpdateFinish = () => {
    const newMax =
      data.find((item) => item.id === itemId)?.newEstimates[platform]?.max ||
      "";

    const updatedData = data.map((item) => {
      if (item.id === itemId) {
        const newMin = Math.min(newMax, item.newEstimates[platform].min);
        return {
          ...item,
          newEstimates: {
            ...item.newEstimates,
            [platform]: {
              ...item.newEstimates[platform],
              min: newMin,
            },
          },
        };
      }
      return item;
    });
    if (newMax !== "") {
      setData(updatedData);
    }
  };

  const handleKeyDown = (e) => {
    if (e.key === "Enter") {
      e.target.blur();
    }
  };

  return (
    <Input
      type="text"
      className="estimate-cell-input"
      placeholder="max"
      value={
        data.find((item) => item.id === itemId)?.newEstimates[platform]?.max
      }
      onChange={handleChange}
      onBlur={handleUpdateFinish}
      onKeyDown={handleKeyDown}
    />
  );
}

function TotalCellInput({ value, placeholder }) {
  return (
    <Input
      type="text"
      value={value}
      placeholder={placeholder}
      className="estimate-cell-input"
      disabled={true}
    />
  );
}

function RateCellInput({ value, onChange }) {
  const formattedValue = value ? value + "$" : "";

  const handleInputChange = (event) => {
    const value = event.target.value;
    const numericInput = value.replace(/[^0-9]/g, "");
    onChange(parseInt(numericInput));
  };

  return (
    <Input
      type="text"
      className="rate-cell-input"
      placeholder="Rate ($)"
      value={formattedValue}
      onChange={handleInputChange}
    />
  );
}

function Table({
  data,
  setData,
  selectedTechnologies,
  selectedRates,
  onAddFeature,
  onDeleteFeature,
  setSelectedTechnologies,
  setSelectedRates,
}) {
  const { technologies } = useTechnologies();

  const platformClasses = classNames("platform", {});
  const technologyClasses = classNames("technology", {});
  const featureClasses = classNames("feature", {});
  const featureDescriptionClasses = classNames("feature-description", {});
  const estimateClasses = classNames("estimate", {});
  const newEstimateClasses = classNames("estimate-new", {});
  const rateClasses = classNames("rate", {});
  const totalClasses = classNames("total", {});
  const newTotalClasses = classNames("total-new", {});
  const addFeatureBtnClasses = classNames("add-feature-btn-cell", {});

  const defaultTechnologyOption = { id: 0, name_of_technology: "Technologies" };

  const renderData = (data) => {
    return data.map((item) => (
      <React.Fragment key={item.id}>
        <tr>
          <td rowSpan={2} className="feature-cell">
            <div className={featureClasses}>
              <SpanWithTooltip
                text={item.name}
                tooltipId={item.id + "-feature-set-tooltip"}
              />
              <DeleteIcon
                onClick={() => {
                  handleDeleteFutureBtnClicked(item.id);
                }}
              />
            </div>
          </td>
          <td className={estimateClasses}>{item.backend_min}</td>
          <td className={estimateClasses}>{item.backend_max}</td>
          <td className={estimateClasses}>{item.frontend_min}</td>
          <td className={estimateClasses}>{item.frontend_max}</td>
          <td className={estimateClasses}>{item.IOS_min}</td>
          <td className={estimateClasses}>{item.IOS_max}</td>
          <td className={estimateClasses}>{item.android_min}</td>
          <td className={estimateClasses}>{item.android_max}</td>
          <td className={estimateClasses}>{item.cross_platform_min}</td>
          <td className={estimateClasses}>{item.cross_platform_max}</td>
        </tr>
        <tr>
          <td className={newEstimateClasses}>
            <MinCellInput
              itemId={item.id}
              platform="backend"
              data={data}
              setData={setData}
            />
          </td>
          <td className={newEstimateClasses}>
            <MaxCellInput
              itemId={item.id}
              platform="backend"
              data={data}
              setData={setData}
            />
          </td>
          <td className={newEstimateClasses}>
            <MinCellInput
              itemId={item.id}
              platform="frontend"
              data={data}
              setData={setData}
            />
          </td>
          <td className={newEstimateClasses}>
            <MaxCellInput
              itemId={item.id}
              platform="frontend"
              data={data}
              setData={setData}
            />
          </td>
          <td className={newEstimateClasses}>
            <MinCellInput
              itemId={item.id}
              platform="ios"
              data={data}
              setData={setData}
            />
          </td>
          <td className={newEstimateClasses}>
            <MaxCellInput
              itemId={item.id}
              platform="ios"
              data={data}
              setData={setData}
            />
          </td>
          <td className={newEstimateClasses}>
            <MinCellInput
              itemId={item.id}
              platform="android"
              data={data}
              setData={setData}
            />
          </td>
          <td className={newEstimateClasses}>
            <MaxCellInput
              itemId={item.id}
              platform="android"
              data={data}
              setData={setData}
            />
          </td>
          <td className={newEstimateClasses}>
            <MinCellInput
              itemId={item.id}
              platform="crossPlatform"
              data={data}
              setData={setData}
            />
          </td>
          <td className={newEstimateClasses}>
            <MaxCellInput
              itemId={item.id}
              platform="crossPlatform"
              data={data}
              setData={setData}
            />
          </td>
        </tr>
        <tr>
          <td rowSpan={1}>
            <div className={featureDescriptionClasses}>
              {item.sub_features.map((subFeature) => (
                <div key={subFeature.id}>
                  <span>•</span>
                  <SpanWithTooltip
                    text={subFeature.name}
                    tooltipId={subFeature.id + "-sub-feature-tooltip"}
                  />
                </div>
              ))}
            </div>
          </td>
          <td className={estimateClasses} colSpan={2} />
          <td className={estimateClasses} colSpan={2} />
          <td className={estimateClasses} colSpan={2} />
          <td className={estimateClasses} colSpan={2} />
          <td className={estimateClasses} colSpan={2} />
        </tr>
      </React.Fragment>
    ));
  };

  const handleAddFutureBtnClicked = () => {
    onAddFeature();
  };

  const handleDeleteFutureBtnClicked = (featureId) => {
    onDeleteFeature(featureId);
  };

  function calculateTotal(array) {
    return array.map((item) => parseInt(item) || 0).reduce((a, b) => a + b, 0);
  }

  return (
    <table>
      <thead>
        <tr>
          <th>Feature Sets</th>
          <th colSpan={10}>Platforms </th>
        </tr>
      </thead>

      <tbody>
        <tr>
          <td className={addFeatureBtnClasses} rowSpan={3}>
            <Button
              secondary
              width="100%"
              height="2.5rem"
              onClick={handleAddFutureBtnClicked}
            >
              Add feature sets
            </Button>
          </td>
          <td className={platformClasses} colSpan={2}>
            Backend
          </td>
          <td className={platformClasses} colSpan={2}>
            Frontend
          </td>
          <td className={platformClasses} colSpan={2}>
            iOS
          </td>
          <td className={platformClasses} colSpan={2}>
            Android
          </td>
          <td className={platformClasses} colSpan={2}>
            Cross-platform
          </td>
        </tr>
        <tr>
          <td className={rateClasses} colSpan={2}>
            <RateCellInput
              value={selectedRates.backend}
              onChange={(newValue) =>
                setSelectedRates({ ...selectedRates, backend: newValue })
              }
            />
          </td>
          <td className={rateClasses} colSpan={2}>
            <RateCellInput
              value={selectedRates.frontend}
              onChange={(newValue) =>
                setSelectedRates({ ...selectedRates, frontend: newValue })
              }
            />
          </td>
          <td className={rateClasses} colSpan={2}>
            <RateCellInput
              value={selectedRates.ios}
              onChange={(newValue) =>
                setSelectedRates({ ...selectedRates, ios: newValue })
              }
            />
          </td>
          <td className={rateClasses} colSpan={2}>
            <RateCellInput
              value={selectedRates.android}
              onChange={(newValue) =>
                setSelectedRates({ ...selectedRates, android: newValue })
              }
            />
          </td>
          <td className={rateClasses} colSpan={2}>
            <RateCellInput
              value={selectedRates.crossPlatform}
              onChange={(newValue) =>
                setSelectedRates({ ...selectedRates, crossPlatform: newValue })
              }
            />
          </td>
        </tr>
        <tr>
          <td className={technologyClasses} colSpan={2}>
            <Dropdown
              options={technologies?.Backend || []}
              onSelect={(selectedOption) =>
                setSelectedTechnologies({
                  ...selectedTechnologies,
                  backend: selectedOption,
                })
              }
              selectedOption={
                selectedTechnologies.backend || defaultTechnologyOption
              }
            />
          </td>
          <td className={technologyClasses} colSpan={2}>
            <Dropdown
              options={technologies?.Frontend || []}
              onSelect={(selectedOption) =>
                setSelectedTechnologies({
                  ...selectedTechnologies,
                  frontend: selectedOption,
                })
              }
              selectedOption={
                selectedTechnologies.frontend || defaultTechnologyOption
              }
            />
          </td>
          <td className={technologyClasses} colSpan={2}>
            <Dropdown
              options={technologies?.IOS || []}
              onSelect={(selectedOption) =>
                setSelectedTechnologies({
                  ...selectedTechnologies,
                  ios: selectedOption,
                })
              }
              selectedOption={
                selectedTechnologies.ios || defaultTechnologyOption
              }
            />
          </td>
          <td className={technologyClasses} colSpan={2}>
            <Dropdown
              options={technologies?.Android || []}
              onSelect={(selectedOption) =>
                setSelectedTechnologies({
                  ...selectedTechnologies,
                  android: selectedOption,
                })
              }
              selectedOption={
                selectedTechnologies.android || defaultTechnologyOption
              }
            />
          </td>
          <td className={technologyClasses} colSpan={2}>
            <Dropdown
              options={technologies?.Cross_platform || []}
              onSelect={(selectedOption) =>
                setSelectedTechnologies({
                  ...selectedTechnologies,
                  crossPlatform: selectedOption,
                })
              }
              selectedOption={
                selectedTechnologies.crossPlatform || defaultTechnologyOption
              }
            />
          </td>
        </tr>
        {renderData(data)}
        <tr className={totalClasses}>
          <td rowSpan={2}>Total</td>
          <td className={estimateClasses}>
            <TotalCellInput
              placeholder="min"
              value={calculateTotal(data.map((item) => item.backend_min))}
            />
          </td>
          <td className={estimateClasses}>
            <TotalCellInput
              placeholder="max"
              value={calculateTotal(data.map((item) => item.backend_max))}
            />
          </td>
          <td className={estimateClasses}>
            <TotalCellInput
              placeholder="min"
              value={calculateTotal(data.map((item) => item.frontend_min))}
            />
          </td>
          <td className={estimateClasses}>
            <TotalCellInput
              placeholder="max"
              value={calculateTotal(data.map((item) => item.frontend_max))}
            />
          </td>
          <td className={estimateClasses}>
            <TotalCellInput
              placeholder="min"
              value={calculateTotal(data.map((item) => item.IOS_min))}
            />
          </td>
          <td className={estimateClasses}>
            <TotalCellInput
              placeholder="max"
              value={calculateTotal(data.map((item) => item.IOS_max))}
            />
          </td>
          <td className={estimateClasses}>
            <TotalCellInput
              placeholder="min"
              value={calculateTotal(data.map((item) => item.android_min))}
            />
          </td>
          <td className={estimateClasses}>
            <TotalCellInput
              placeholder="max"
              value={calculateTotal(data.map((item) => item.android_max))}
            />
          </td>
          <td className={estimateClasses}>
            <TotalCellInput
              placeholder="min"
              value={calculateTotal(
                data.map((item) => item.cross_platform_min)
              )}
            />
          </td>
          <td className={estimateClasses}>
            <TotalCellInput
              placeholder="max"
              value={calculateTotal(
                data.map((item) => item.cross_platform_max)
              )}
            />
          </td>
        </tr>
        <tr className={newTotalClasses}>
          <td className={newEstimateClasses}>
            <TotalCellInput
              placeholder="min"
              value={calculateTotal(
                data.map((item) => item.newEstimates.backend.min)
              )}
            />
          </td>
          <td className={newEstimateClasses}>
            <TotalCellInput
              placeholder="max"
              value={calculateTotal(
                data.map((item) => item.newEstimates.backend.max)
              )}
            />
          </td>
          <td className={newEstimateClasses}>
            <TotalCellInput
              placeholder="min"
              value={calculateTotal(
                data.map((item) => item.newEstimates.frontend.min)
              )}
            />
          </td>
          <td className={newEstimateClasses}>
            <TotalCellInput
              placeholder="max"
              value={calculateTotal(
                data.map((item) => item.newEstimates.frontend.max)
              )}
            />
          </td>
          <td className={newEstimateClasses}>
            <TotalCellInput
              placeholder="min"
              value={calculateTotal(
                data.map((item) => item.newEstimates.ios.min)
              )}
            />
          </td>
          <td className={newEstimateClasses}>
            <TotalCellInput
              placeholder="max"
              value={calculateTotal(
                data.map((item) => item.newEstimates.ios.max)
              )}
            />
          </td>
          <td className={newEstimateClasses}>
            <TotalCellInput
              placeholder="min"
              value={calculateTotal(
                data.map((item) => item.newEstimates.android.min)
              )}
            />
          </td>
          <td className={newEstimateClasses}>
            <TotalCellInput
              placeholder="max"
              value={calculateTotal(
                data.map((item) => item.newEstimates.android.max)
              )}
            />
          </td>
          <td className={newEstimateClasses}>
            <TotalCellInput
              placeholder="min"
              value={calculateTotal(
                data.map((item) => item.newEstimates.crossPlatform.min)
              )}
            />
          </td>
          <td className={newEstimateClasses}>
            <TotalCellInput
              placeholder="max"
              value={calculateTotal(
                data.map((item) => item.newEstimates.crossPlatform.max)
              )}
            />
          </td>
        </tr>
      </tbody>
    </table>
  );
}

export default Table;
