import { useEffect } from "react";
import { toast, Toaster, ToastBar, useToasterStore } from "react-hot-toast";

import { ReactComponent as WarningIcon } from "../assets/icons/warning_icon.svg";
import { ReactComponent as SuccessIcon } from "../assets/icons/success_icon.svg";
import { ReactComponent as CloseIcon } from "../assets/icons/close_icon.svg";

function CustomToaster({ ...rest }) {
  const { toasts } = useToasterStore();
  const TOAST_LIMIT = 1;

  useEffect(() => {
    toasts
      .filter((t) => t.visible)
      .filter((_, i) => i >= TOAST_LIMIT)
      .forEach((t) => toast.dismiss(t.id));
  }, [toasts]);

  return (
    <Toaster
      containerClassName="toaster"
      position="top-right"
      gutter={12}
      toastOptions={{
        success: {
          duration: 3000,
          icon: <SuccessIcon />,
          style: {
            backgroundColor: "#5351cf",
          },
        },
        error: {
          duration: 3000,
          icon: <WarningIcon />,
          style: {
            backgroundColor: "#FF4242",
          },
        },
        style: {
          fontFamily: "Montserrat",
          fontSize: "1rem",
          fontStyle: "normal",
          fontWeight: 500,
          padding: "1.5rem 1rem",
          color: "#F0F0F0",
          maxWidth: "45rem",
        },
      }}
      {...rest}
    >
      {(t) => (
        <ToastBar toast={t}>
          {({ icon, message }) => (
            <div className="toast-bar">
              {icon}
              {message}
              {t.type !== "loading" && (
                <CloseIcon
                  onClick={() => toast.dismiss(t.id)}
                  cursor="pointer"
                />
              )}
            </div>
          )}
        </ToastBar>
      )}
    </Toaster>
  );
}

export default CustomToaster;
