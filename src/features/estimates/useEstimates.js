import { useMutation } from "@tanstack/react-query";

import { getEstimates as getEstimatesApi } from "../../services/EstimateService";

export function useEstimates() {
  const { mutate: getEstimates, isLoading } = useMutation({
    mutationFn: ({
      client_name,
      client_email,
      technologies,
      rates,
      features,
    }) =>
      getEstimatesApi({
        client_name,
        client_email,
        technologies,
        rates,
        features,
      }),
  });

  return { getEstimates, isLoading };
}
