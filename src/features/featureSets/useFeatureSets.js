import { useQuery } from "@tanstack/react-query";

import { getFeatureSets } from "../../services/FeatureSetService";

export function useFeatureSets() {
  const {
    data: features,
    isLoading,
    isError,
  } = useQuery({
    queryKey: ["features"],
    queryFn: getFeatureSets,
  });

  return { isError, isLoading, features };
}
