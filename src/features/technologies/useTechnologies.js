import { useQuery } from "@tanstack/react-query";

import { getTechnologies } from "../../services/TechnologyService";

export function useTechnologies() {
  const {
    data: technologies,
    isLoading,
    isError,
  } = useQuery({
    queryKey: ["technologies"],
    queryFn: getTechnologies,
  });

  return { isError, isLoading, technologies };
}
