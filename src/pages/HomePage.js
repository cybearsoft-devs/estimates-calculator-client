import { useState } from "react";
import { toast } from "react-hot-toast";

import { useFeatureSets } from "../features/featureSets/useFeatureSets";
import { useEstimates } from "../features/estimates/useEstimates";

import Table from "../components/Table";
import GetEstimatesForm from "../components/GetEstimatesForm";
import AddFeatureSetPopup from "../components/AddFeatureSetPopup";
import DeleteFeatureSetPopup from "../components/DeleteFeatureSetPopup";

function HomePage() {
  const { features } = useFeatureSets();
  const { getEstimates, isLoading } = useEstimates();

  const [showAddFeaturePopup, setShowAddFeaturePopup] = useState(false);
  const [showDeleteFeaturePopup, setShowDeleteFeaturePopup] = useState(false);

  const [selectedFeatures, setSelectedFeatures] = useState([]);
  const [deleteSelectedFeatureId, setDeleteSelectedFeatureId] = useState(null);

  const [selectedTechnologies, setSelectedTechnologies] = useState({
    backend: null,
    frontend: null,
    ios: null,
    android: null,
    crossPlatform: null,
  });

  const [selectedRates, setSelectedRates] = useState({
    backend: "",
    frontend: "",
    ios: "",
    android: "",
    crossPlatform: "",
  });

  const handleAddFeaturePopupShow = () => {
    setShowAddFeaturePopup(true);
  };

  const handleDeleteFeaturePopupShow = (selectedFeatureId) => {
    setDeleteSelectedFeatureId(selectedFeatureId);
    setShowDeleteFeaturePopup(true);
  };

  const handleAddFeatureSet = (newSelectedFeatures) => {
    setSelectedFeatures(newSelectedFeatures);
    setShowAddFeaturePopup(false);
  };

  const handleDeleteFeature = () => {
    if (deleteSelectedFeatureId !== null) {
      setSelectedFeatures((prevSelectedFeatures) =>
        prevSelectedFeatures.filter(
          (feature) => feature.id !== deleteSelectedFeatureId
        )
      );
    }
    setDeleteSelectedFeatureId(null);
    setShowDeleteFeaturePopup(false);
  };

  const handleCancelAddFeature = () => {
    setShowAddFeaturePopup(false);
  };

  const handleCancelDeleteFeature = () => {
    setDeleteSelectedFeatureId(null);
    setShowDeleteFeaturePopup(false);
  };

  const handleFormSubmit = (name, email, onSuccess) => {
    getEstimates(
      {
        client_name: name,
        client_email: email,
        rates: selectedRates,
        features: selectedFeatures,
        technologies: selectedTechnologies,
      },
      {
        onSuccess: () => {
          toast.success(
            "The email was successfully sent to the provided email address"
          );
          onSuccess();
        },
      }
    );
  };

  return (
    <>
      {showAddFeaturePopup && (
        <AddFeatureSetPopup
          features={features || []}
          selectedFeatures={selectedFeatures}
          isOpen={showAddFeaturePopup}
          onAdd={handleAddFeatureSet}
          onCancel={handleCancelAddFeature}
        />
      )}
      {showDeleteFeaturePopup && (
        <DeleteFeatureSetPopup
          isOpen={showDeleteFeaturePopup}
          onYes={handleDeleteFeature}
          onNo={handleCancelDeleteFeature}
        />
      )}
      <div className="homepage">
        <h1>Get a Custom Estimate</h1>
        <div className="homepage-content">
          <span>
            Get a customized estimate and compare the timelines, costs, and
            resources needed to build your product with our development services
            versus hiring in-house.
          </span>
          <Table
            data={selectedFeatures}
            setData={setSelectedFeatures}
            selectedTechnologies={selectedTechnologies}
            selectedRates={selectedRates}
            onAddFeature={handleAddFeaturePopupShow}
            onDeleteFeature={handleDeleteFeaturePopupShow}
            setSelectedTechnologies={setSelectedTechnologies}
            setSelectedRates={setSelectedRates}
          />
        </div>
        <GetEstimatesForm onSubmit={handleFormSubmit} isLoading={isLoading} />
      </div>
    </>
  );
}

export default HomePage;
