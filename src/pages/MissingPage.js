function MissingPage() {
  return (
    <div className="page__container">
      <div className="missing-page">
        <h1>Oops!</h1>
        <h2>Page Not Found</h2>
      </div>
    </div>
  );
}

export default MissingPage;
