import api from "./APIService";

export async function getEstimates({
  client_name,
  client_email,
  technologies,
  rates,
  features,
}) {
  const mappedFeatures = {
    features: features.map((feature) => ({
      id: feature.id,
      name: feature?.name || "",
      backend_min: feature?.newEstimates.backend?.min || 0,
      backend_max: feature?.newEstimates.backend?.max || 0,
      frontend_min: feature?.newEstimates.frontend?.min || 0,
      frontend_max: feature?.newEstimates.frontend?.max || 0,
      IOS_min: feature?.newEstimates.ios?.min || 0,
      IOS_max: feature?.newEstimates.ios?.max || 0,
      android_min: feature?.newEstimates.android?.min || 0,
      android_max: feature?.newEstimates.android?.max || 0,
      cross_platform_min: feature?.newEstimates.crossPlatform?.min || 0,
      cross_platform_max: feature?.newEstimates.crossPlatform?.max || 0,
    })),
  };

  const mappedTechnologies = {
    backend_technology: technologies.backend?.name_of_technology || "",
    frontend_technology: technologies.frontend?.name_of_technology || "",
    iOS_technology: technologies.ios?.name_of_technology || "",
    android_technology: technologies.android?.name_of_technology || "",
    cross_platform_technology:
      technologies.crossPlatform?.name_of_technology || "",
  };

  const mappedRates = {
    backend_rate: rates.backend || 0,
    frontend_rate: rates.frontend || 0,
    iOS_rate: rates.ios || 0,
    android_rate: rates.android || 0,
    cross_platform_rate: rates.crossPlatform || 0,
  };

  if (mappedFeatures.features.length === 0) {
    throw new Error("Please, choose at least one Feature Set");
  }

  try {
    const response = await api.post(
      "/estimate_calculation/retrieve_results/",
      JSON.stringify({
        client_name,
        client_email,
        ...mappedRates,
        ...mappedFeatures,
        ...mappedTechnologies,
      })
    );
    return response.data;
  } catch (error) {
    throw error;
  }
}
