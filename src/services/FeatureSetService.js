import api from "./APIService";

export async function getFeatureSets() {
  try {
    const response = await api.get("/estimate_calculation/list_of_features/");
    return response.data;
  } catch (error) {
    throw error;
  }
}
