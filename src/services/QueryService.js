import { QueryClient } from "@tanstack/react-query";
import { toast } from "react-hot-toast";

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      staleTime: 120000,
      retry: false,
      onError: (error) => {
        toast.error(error.message);
      },
    },
    mutations: {
      retry: false,
      onError: (error) => {
        toast.error(error.message);
      },
    },
  },
});

export default queryClient;
