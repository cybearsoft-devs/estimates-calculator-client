import api from "./APIService";

export async function getTechnologies() {
  try {
    const response = await api.get(
      "/estimate_calculation/list_of_technologies"
    );

    const technologies = response.data;

    const mappedData = {};

    technologies.forEach((tech) => {
      tech.types_of_development.forEach((type) => {
        const typeName = type.name;

        if (!mappedData[typeName]) {
          mappedData[typeName] = [];
        }

        mappedData[typeName].push({
          id: tech.id,
          name_of_technology: tech.name_of_technology,
        });
      });
    });
    return mappedData;
  } catch (error) {
    throw error;
  }
}
