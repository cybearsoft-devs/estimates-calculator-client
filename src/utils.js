export function isValidEmail(email) {
  const emailRegex = /^[^\s@]+@[^\s@]+\.[a-z]+$/i;
  return emailRegex.test(email);
}

export function isTextOverflowing(element) {
  return element.scrollWidth > element.clientWidth;
}
